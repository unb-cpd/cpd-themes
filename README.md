CPD
================
Temas Noosfero para o Portal CPD.


Instruções de uso
=================

1. Apague a pasta temas do Noosfero. Na pasta raiz do noosfero execute o comando:

```bash
rm -rf noosfero/public/designs/themes
```

2. Clonar diretório na pasta designs

```bash
git clone https://gitlab.com/unb-cpd/cpd-themes.git themes
```
