$(document).ready(function () {

  // document.getElementById('theme-header').appendChild(
  //   document.getElementById('top-search')
  // );

  var user = $('#user');
  var siteTitle = $('#site-title');
  var navigation = $('#navigation');
  var topSearch = $('#top-search');
  var newParent = $('#content-header');

  newParent.append(siteTitle);
  newParent.append(topSearch);
  newParent.append(user);
  newParent.append(navigation);

  // drop-down just in link-list-block
  $('.link-list-block').children().children().children('h3').prop('class', 'block-title block-title-dropdown');

  // added slide toggle to block titles
  $('.block-title-dropdown').click(function(){
    if ( $(this).parent().parent().parent().is('.link-list-block') ){
      $(this).parent().children("ul").slideToggle();
      if ( $(this).is('.has-hidden-subelements') ) {
        $(this).prop('class', 'block-title block-title-dropdown');
      } else {
        $(this).prop('class', 'block-title block-title-dropdown has-hidden-subelements');
      }
    }

  // select all the `.child` elements and stop the propagation of click events on the elements
  }).children('.child').click(function (event) {
    event.stopPropagation();
  });

});
